import tensorflow as tf
#import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt


# HyperParamters
# write parameters here.


# Data
# write data here.


# Graph
graph = tf.Graph()
with graph.as_default():
    # write model here.
    
    init = tf.global_variables_initializer()


# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    # write learning loop here.
    #for x_val in x_values:
    #    result = sess.run(add1, feed_dict={x: x_val})
    #    print(result)

# close writer
writer.close()
