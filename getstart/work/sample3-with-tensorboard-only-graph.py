import tensorflow as tf

graph = tf.Graph()
with graph.as_default():
    # Model parameters
    W = tf.Variable([.3], dtype=tf.float32)
    b = tf.Variable([-.3], dtype=tf.float32)
    # Model input and output
    x = tf.placeholder(tf.float32)
    linear_model = W*x + b
    y = tf.placeholder(tf.float32)
    
    # loss
    loss = tf.reduce_sum(tf.square(linear_model - y)) # sum of the squares
    # optimizer
    optimizer = tf.train.GradientDescentOptimizer(0.01)
    train = optimizer.minimize(loss)
    
    # initialize task
    init = tf.global_variables_initializer()

# training data
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# training loop
sess = tf.Session(graph=graph)
sess.run(init) # reset values to wrong
for i in range(1000):
  sess.run(train, {x: x_train, y: y_train})

# evaluate training accuracy
curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))

# close writer
writer.close()