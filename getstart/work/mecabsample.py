import MeCab
import os
tagger = MeCab.Tagger('-d {0}/opt/mecab/lib/mecab/dic/mecab-ipadic-neologd'.format(os.environ['HOME']))

input = '今週末は金剛山に登りに行って、そのあと新世界かどっかで打ち上げしましょ。'

result = tagger.parse(input)
print(result)

node = tagger.parseToNode(input)
target_parts_of_speech = ('名詞', )
while node:
    if node.feature.split(',')[0] in target_parts_of_speech:
        print(node.surface)
    node = node.next
    