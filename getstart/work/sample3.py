import tensorflow as tf

''' step1
W = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
x = tf.placeholder(tf.float32)
linear_model = W * x + b

init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)

print(sess.run(linear_model, {x: [1, 2, 3, 4]}))
'''

''' step2
W = tf.Variable([-1.], dtype=tf.float32)
b = tf.Variable([1.], dtype=tf.float32)
x = tf.placeholder(tf.float32)
linear_model = W * x + b
y = tf.placeholder(tf.float32)
squared_deltas = tf.square(linear_model - y)
loss = tf.reduce_sum(squared_deltas)

init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)

print(sess.run(loss, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))
'''

''' step3
'''
W = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
x = tf.placeholder(tf.float32)
linear_model = W * x + b

y = tf.placeholder(tf.float32)
loss = tf.reduce_sum(tf.square(linear_model - y))

optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)

x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]

for i in range(1000):
    sess.run(train, {x: x_train, y: y_train})
print(sess.run([W, b]))

# evaluate training accuracy
curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))
