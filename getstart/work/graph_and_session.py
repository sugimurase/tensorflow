import tensorflow as tf

graph1 = tf.Graph()
with graph1.as_default():
    g1_variable = tf.Variable(42, name='val_a')
    g1_initialize = tf.global_variables_initializer()
    g1_assign = g1_variable.assign(13)

graph2 = tf.Graph()
with graph2.as_default():
    g2_variable = tf.Variable(100, name='val_a')
    g2_initialize = tf.global_variables_initializer()

with tf.Session(graph=graph1) as sess:
    sess.run(g1_initialize)
    sess.run(g1_assign)
    print(sess.run(g1_variable))

with tf.Session(graph=graph2) as sess:
    sess.run(g2_initialize)
    print(sess.run(g2_variable))
