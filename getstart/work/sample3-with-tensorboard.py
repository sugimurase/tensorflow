import tensorflow as tf


# Graph
graph = tf.Graph()
with graph.as_default():
        
    # Model input and output
    with tf.name_scope('input'):
        x = tf.placeholder(tf.float32, name='x')
    with tf.name_scope('teacher'):
        y = tf.placeholder(tf.float32, name='y')
    
    # Model parameters
    with tf.name_scope('model'):
        with tf.name_scope('hidden'):
            W = tf.Variable([.3], dtype=tf.float32, name='W')
            tf.summary.histogram('hist W', W)
            b = tf.Variable([-.3], dtype=tf.float32, name='b')
            tf.summary.histogram('hist b', b)
        linear_model = W * x + b

    # loss
    with tf.name_scope('loss'):
        loss = tf.reduce_sum(tf.square(linear_model - y)) # sum of the squares
        tf.summary.scalar('Loss', loss)

    # optimizer
    with tf.name_scope('optimizer'):
        optimizer = tf.train.GradientDescentOptimizer(0.01)
        train = optimizer.minimize(loss)
    
    # initialize task
    with tf.name_scope('initialize'):
        init = tf.global_variables_initializer()
    
    # merge summaries
    merged_summary = tf.summary.merge_all()


# Data
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    # initialize graph first
    sess.run(init)

    # training loop
    for i in range(1000):
        summary, result = sess.run([merged_summary, train], {x: x_train, y: y_train})
        if i % 10 == 0:
            writer.add_summary(summary, i)

    # print result
    #   parameters
    print(sess.run([W, b]))
    #   evaluate training accuracy
    curr_W, curr_b, curr_loss = sess.run([W, b, loss], {x: x_train, y: y_train})
    print("W: %s b: %s loss: %s"%(curr_W, curr_b, curr_loss))

# close writer
writer.close()