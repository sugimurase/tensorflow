import tensorflow as tf
#import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('../MNIST_data/', one_hot=True)

# Hyper Parameter
alpha = 1e-4
totalstep = 2000
batchsize = 50


# Data
# write data here.


# Graph
graph = tf.Graph()
with graph.as_default():
    def weight_variable(shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.1), name='weight')

    def bias_variable(shape):
        return tf.Variable(tf.constant(0.1, shape=shape), name='bias')

    def conv2d(x, W):
        return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME', name='conv2d')

    def max_pool_2x2(x):
        return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                              strides=[1, 2, 2, 1], padding='SAME', name='pooling')

    with tf.name_scope('input'):
        x = tf.placeholder(tf.float32, [None, 784], name='x')

    with tf.name_scope('teacher'):
        y_ = tf.placeholder(tf.float32, [None, 10], name='y_')
        
    with tf.name_scope('reshape'):
        x_image = tf.reshape(x, [-1, 28, 28, 1])
        tf.summary.image('preprocess', x_image, 10)
    
    with tf.name_scope('conv1'):
        W_conv1 = weight_variable([5, 5, 1, 32])
        b_conv1 = bias_variable([32])
        h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
        
    with tf.name_scope('pool1'):
        h_pool1 = max_pool_2x2(h_conv1)
        
    with tf.name_scope('conv2'):
        W_conv2 = weight_variable([5, 5, 32, 64])
        b_conv2 = bias_variable([64])
        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        
    with tf.name_scope('pool2'):
        h_pool2 = max_pool_2x2(h_conv2)
    
    with tf.name_scope('fc1'):
        W_fc1 = weight_variable([7 * 7 * 64, 1024])
        b_fc1 = bias_variable([1024])
        h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)
        
    with tf.name_scope('dropout'):
        keep_prob = tf.placeholder(tf.float32)
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    with tf.name_scope('fc2'):
        W_fc2 = weight_variable([1024, 10])
        b_fc2 = bias_variable([10])
        y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    with tf.name_scope('loss'):
        cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
            labels=y_, logits=y_conv))
        tf.summary.scalar('cross entropy', cross_entropy)

    with tf.name_scope('train'):
        train_step = tf.train.AdamOptimizer(alpha).minimize(cross_entropy)
    
    with tf.name_scope('evaluate'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar('accuracy', accuracy)

    init = tf.global_variables_initializer()
    
    merged_summary = tf.summary.merge_all()


# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    for i in range(totalstep):
        batch_xs, batch_ys = mnist.train.next_batch(batchsize)
        if i % 100 == 0:
            train_accuracy = accuracy.eval(feed_dict={
                x: batch_xs, y_: batch_ys, keep_prob: 1.0})
            print('step {}, training accuracyy {}'.format(i, train_accuracy))
        summary, result = sess.run([merged_summary, train_step],
            feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 0.5})
        writer.add_summary(summary, i)

    print('test accuracy {}'.format(accuracy.eval(feed_dict={
        x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0})))

# close writer
writer.close()
