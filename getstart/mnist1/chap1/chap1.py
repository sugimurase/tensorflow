import tensorflow as tf
#import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('../MNIST_data/', one_hot=True)

# Hyper Parameter
alpha = 0.5
totalstep = 1000
batchsize = 100

# Graph
graph = tf.Graph()
with graph.as_default():
    with tf.name_scope('input'):
        x = tf.placeholder(tf.float32, [None, 784], name='x')

    with tf.name_scope('output'):
        with tf.name_scope('hidden'):
            W = tf.Variable(tf.zeros([784, 10]), name='W')
            b = tf.Variable(tf.zeros([10]), name='b')
        y = tf.nn.softmax(tf.matmul(x, W) + b, name='y')

    with tf.name_scope('teacher'):
        y_ = tf.placeholder(tf.float32, [None, 10], name='y_')

    with tf.name_scope('loss'):
        cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y),
            reduction_indices=[1]))
        tf.summary.scalar('cross entropy', cross_entropy)

    with tf.name_scope('train'):
        train_step = tf.train.GradientDescentOptimizer(alpha).minimize(
            cross_entropy)
    
    with tf.name_scope('evaluate'):
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar('accuracy', accuracy)

    init = tf.global_variables_initializer()
    
    merged_summary = tf.summary.merge_all()

# Data
# write data here.


# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    for i in range(totalstep):
        batch_xs, batch_ys = mnist.train.next_batch(batchsize)
        summary, result = sess.run([merged_summary, train_step],
            feed_dict={x: batch_xs, y_: batch_ys})
        #print(result)
        if i % 10 == 0:
            writer.add_summary(summary, i)

    print(sess.run(accuracy, feed_dict={x: mnist.test.images,
        y_: mnist.test.labels}))

# close writer
writer.close()
