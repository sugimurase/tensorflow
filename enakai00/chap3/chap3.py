import tensorflow as tf
import math
import numpy as np
from numpy.random import rand, multivariate_normal
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# dataset
class Dataset():
    def __init__(self):
        variance = 40
        n1, n2, n3 = 50, 40, 30
        mu1, mu2, mu3 = [20,20], [0,20], [20, 0]
        cov1 = np.array([[variance,0],[0,variance]])
        cov2 = np.array([[variance,0],[0,variance]])
        cov3 = np.array([[variance,0],[0,variance]])
        data1 = multivariate_normal(mu1,cov1,n1)
        data2 = multivariate_normal(mu2,cov2,n2)
        data3 = multivariate_normal(mu3,cov3,n3)
        data = np.r_[np.c_[data1,np.ones(n1)],
                     np.c_[data2,np.zeros(n2)],
                     np.c_[data3,np.zeros(n3)]]
        np.random.shuffle(data)

        self.test_data, self.test_label = np.hsplit(data[0:20],[2])
        self.train_data, self.train_label = np.hsplit(data[20:],[2])
        self.index = 0

    def next_batch(self, n):
        if self.index + n > len(self.train_data):
            self.index = 0
        data = self.train_data[self.index:self.index+n]
        label = self.train_label[self.index:self.index+n]
        self.index += n
        return data, label

def plot_result(dataset, x, y, weight, bias, mult):
    fig = plt.figure()
    subplot1 = fig.add_subplot(1,2,1)
    subplot2 = fig.add_subplot(1,2,2)

    data0_x, data0_y, data1_x, data1_y = [], [], [], []
    for i in range(len(dataset.train_data)):
        if dataset.train_label[i][0] == 0:
            data0_x.append(dataset.train_data[i][0])
            data0_y.append(dataset.train_data[i][1])
        else:
            data1_x.append(dataset.train_data[i][0])
            data1_y.append(dataset.train_data[i][1])
    subplot1.scatter(data0_x, data0_y, marker='x', color='blue')
    subplot1.scatter(data1_x, data1_y, marker='o', color='blue')
    subplot2.scatter(data0_x, data0_y, marker='x', color='blue')
    subplot2.scatter(data1_x, data1_y, marker='o', color='blue')

    data0_x, data0_y, data1_x, data1_y = [], [], [], []
    for i in range(len(dataset.test_data)):
        if dataset.test_label[i][0] == 0:
            data0_x.append(dataset.test_data[i][0])
            data0_y.append(dataset.test_data[i][1])
        else:
            data1_x.append(dataset.test_data[i][0])
            data1_y.append(dataset.test_data[i][1])
    subplot1.scatter(data0_x, data0_y, marker='x', color='red')
    subplot1.scatter(data1_x, data1_y, marker='o', color='red')
    subplot2.scatter(data0_x, data0_y, marker='x', color='red')
    subplot2.scatter(data1_x, data1_y, marker='o', color='red')

    xs, ys = np.hsplit(dataset.train_data,[1])
    xmin, xmax = xs.min(), xs.max()
    ymin, ymax = ys.min(), ys.max()
    subplot1.set_ylim([ymin-1, ymax+1])
    subplot1.set_xlim([xmin-1, xmax+1])
    subplot2.set_ylim([ymin-1, ymax+1])
    subplot2.set_xlim([xmin-1, xmax+1])

    RES = 80
    X = np.linspace(xmin-1,xmax+1,RES)
    Y = np.linspace(ymin-1,ymax+1,RES)
    field = [[xx,yy] for yy in Y for xx in X]
    a_k = y.eval(session=sess, feed_dict={x: field})
    a_k = a_k.reshape(len(Y), len(X))
    subplot1.imshow(np.sign(a_k-0.5), extent=(xmin-1,xmax+1,ymax+1,ymin-1),
                    alpha=0.4)
    subplot2.imshow(a_k, extent=(xmin-1,xmax+1,ymax+1,ymin-1),
                    alpha=0.4, vmin=0, vmax=1, cmap=plt.cm.gray_r)

    linex = np.arange(xmin-1, xmax+1)
    for i in range(len(bias[0])):
        wx, wy, b = weight[0][i], weight[1][i], bias[0][i]
        liney = - linex * wx/wy - b*mult/wy
        subplot2.plot(linex, liney, color='red')
    #plt.show()
    plt.savefig('chap3.png')

# Main
if __name__ == '__main__':
    graph = tf.Graph()
    with graph.as_default():
        dataset = Dataset()
        mult = dataset.train_data.flatten().mean()
        hidden1_units = 2
        
        # Create the model
        with tf.name_scope('input'):
            x = tf.placeholder(tf.float32, [None, 2])
    
        with tf.name_scope('hidden1'):
            w0 = tf.Variable(
                tf.truncated_normal([2, hidden1_units],stddev=1.0/math.sqrt(2.0)),
                name='weights')
            b0 = tf.Variable(tf.zeros([1, hidden1_units]), name='biases')
            # hidden1 = tf.nn.relu(tf.matmul(x, w0) + b0*mult)
            hidden1 = tf.nn.tanh(tf.matmul(x, w0) + b0*mult)
    
            # Logging data for TensorBoard
            _ = tf.summary.histogram('hidden_weight', w0)
            _ = tf.summary.histogram('hidden_bias', b0)
    
        with tf.name_scope('output'):
            w = tf.Variable(tf.zeros([hidden1_units,1]), name='weights')
            b = tf.Variable(tf.zeros([1]), name='biases')
            y = tf.nn.sigmoid(tf.matmul(hidden1, w) + b*mult)
    
            # Logging data for TensorBoard
            _ = tf.summary.histogram('output_weight', w)
            _ = tf.summary.histogram('output_bias', b)
    
        with tf.name_scope('teacher'):
            y_ = tf.placeholder(tf.float32, [None, 1])
            
        with tf.name_scope('loss'):
            log_probability = tf.reduce_sum(y_ * tf.log(y) + (1 - y_) * tf.log(1 - y))
            
        with tf.name_scope('optimizer'):
            optimizer = tf.train.GradientDescentOptimizer(0.001)
        
        with tf.name_scope('train'):
            train_step = optimizer.minimize(-log_probability)
            
        with tf.name_scope('evaluation'):
            correct_prediction = tf.equal(tf.sign(y - 0.5), tf.sign(y_ - 0.5))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        
        # Logging data for TensorBoard
        _ = tf.summary.histogram('probability of training data', y)
        _ = tf.summary.scalar('log_probability', log_probability)
        _ = tf.summary.scalar('accuracy', accuracy)
        merged_summary = tf.summary.merge_all()
        
        # initializer
        init = tf.global_variables_initializer()
    
    # prepare directory for TensorBoard logs, and open writer
    logdir = 'logs'
    if tf.gfile.Exists(logdir):
        tf.gfile.DeleteRecursively(logdir)
    tf.gfile.MakeDirs(logdir)
    writer = tf.summary.FileWriter(logdir, graph)
    
    # Train
    with tf.Session(graph=graph) as sess:
        sess.run(init)
        for i in range(500):
            batch_xs, batch_ys = dataset.next_batch(100)
            feed = {x: batch_xs, y_: batch_ys}
            sess.run(train_step, feed_dict=feed)
        
            feed = {x: dataset.test_data, y_: dataset.test_label}
            summary_str, lp, acc = sess.run(
                [merged_summary, log_probability, accuracy], feed_dict=feed)
            writer.add_summary(summary_str, i)
            print('LogProbability and Accuracy at step %s: %s, %s' % (i, lp, acc))
    
        plot_result(dataset, x, y, w0.eval(), b0.eval(), mult)

    # close writer
    writer.close()
