import tensorflow as tf
import math
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import io

# dataset
class Dataset():
    def __init__(self):
        n1, n2, n3, n4 = 30, 30, 30, 30
        mu1, mu2, mu3, mu4 = [-10,-10], [10,10], [-10, 10], [10,-10]
        cov1 = np.array([[10,0],[0,10]])
        cov2 = np.array([[10,0],[0,10]])
        cov3 = np.array([[10,0],[0,10]])
        cov4 = np.array([[10,0],[0,10]])
        data1 = np.random.multivariate_normal(mu1,cov1,n1)
        data2 = np.random.multivariate_normal(mu2,cov2,n2)
        data3 = np.random.multivariate_normal(mu3,cov3,n3)
        data4 = np.random.multivariate_normal(mu4,cov4,n4)
        data = np.r_[np.c_[data1,np.ones(n1)],
                     np.c_[data2,np.ones(n2)],
                     np.c_[data3,np.zeros(n3)],
                     np.c_[data4,np.zeros(n4)]]
        np.random.shuffle(data)

        self.test_data, self.test_label = np.hsplit(data[0:20],[2])
        self.train_data, self.train_label = np.hsplit(data[20:],[2])
        self.index = 0

    def next_batch(self, n):
        if self.index + n > len(self.train_data):
                self.index = 0
        data = self.train_data[self.index:self.index+n]
        label = self.train_label[self.index:self.index+n]
        self.index += n
        return data, label

def plot_result(sess, dataset, x, y, subplot1, subplot2, weight, bias, mult):
    data0_x, data0_y, data1_x, data1_y = [], [], [], []
    for i in range(len(dataset.train_data)):
        if dataset.train_label[i][0] == 0:
            data0_x.append(dataset.train_data[i][0])
            data0_y.append(dataset.train_data[i][1])
        else:
            data1_x.append(dataset.train_data[i][0])
            data1_y.append(dataset.train_data[i][1])
    subplot1.scatter(data0_x, data0_y, marker='x', color='blue')
    subplot1.scatter(data1_x, data1_y, marker='o', color='blue')
    subplot2.scatter(data0_x, data0_y, marker='x', color='blue')
    subplot2.scatter(data1_x, data1_y, marker='o', color='blue')

    data0_x, data0_y, data1_x, data1_y = [], [], [], []
    for i in range(len(dataset.test_data)):
        if dataset.test_label[i][0] == 0:
            data0_x.append(dataset.test_data[i][0])
            data0_y.append(dataset.test_data[i][1])
        else:
            data1_x.append(dataset.test_data[i][0])
            data1_y.append(dataset.test_data[i][1])
    subplot1.scatter(data0_x, data0_y, marker='x', color='red')
    subplot1.scatter(data1_x, data1_y, marker='o', color='red')
    subplot2.scatter(data0_x, data0_y, marker='x', color='red')
    subplot2.scatter(data1_x, data1_y, marker='o', color='red')

    xs, ys = np.hsplit(dataset.train_data,[1])
    xmin, xmax = xs.min(), xs.max()
    ymin, ymax = ys.min(), ys.max()
    subplot1.set_ylim([ymin-1, ymax+1])
    subplot1.set_xlim([xmin-1, xmax+1])
    subplot2.set_ylim([ymin-1, ymax+1])
    subplot2.set_xlim([xmin-1, xmax+1])

    RES = 80
    X = np.linspace(xmin-1,xmax+1,RES)
    Y = np.linspace(ymin-1,ymax+1,RES)
    field = [[xx,yy] for yy in Y for xx in X]
    a_k = y.eval(session=sess, feed_dict={x: field})
    a_k = a_k.reshape(len(Y), len(X))
    subplot1.imshow(np.sign(a_k-0.5), extent=(xmin-1,xmax+1,ymax+1,ymin-1),
                    alpha=0.4)
    subplot2.imshow(a_k, extent=(xmin-1,xmax+1,ymax+1,ymin-1),
                    alpha=0.4, vmin=0, vmax=1, cmap=plt.cm.gray_r)

    linex = np.arange(xmin-1, xmax+1)
    for i in range(len(bias[0])):
        wx, wy, b = weight[0][i], weight[1][i], bias[0][i]
        liney = - linex * wx/wy - b*mult/wy
        subplot2.plot(linex, liney, color='red')

# plot
def plot(xs, ys, params):
    plt.plot(xs, ys, 'o', label='Data Points')
    for p in params:
        best_fit = []
        a = p['a']
        b = p['b']
        name = p['name']
        for i in xs:
            y = a * i + b
            best_fit.append(y.reshape(-1, ))
        plt.plot(xs, best_fit, '-', label='Best fit line {}'.format(p['name']), linewidth=3)
    plt.legend(loc='upper left')
    plt.title('Sepal Length vs Pedal Width')
    plt.xlabel('Pedal Width')
    plt.ylabel('Sepal Length')
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    return buf

def single_layer(dataset, subplot1, subplot2):
    with tf.Graph().as_default():
        mult = dataset.train_data.flatten().mean()
        hidden1_units = 2

        # Create the model
        with tf.name_scope('input'):
            x = tf.placeholder(tf.float32, [None, 2])

        with tf.name_scope('hidden1'):
            w1 = tf.Variable(
                tf.truncated_normal([2, hidden1_units],stddev=1.0/math.sqrt(2.0)),
                name='weights')
            b1 = tf.Variable(tf.zeros([1, hidden1_units]), name='biases')
            hidden1 = tf.nn.tanh(tf.matmul(x, w1) + b1*mult)

            # Logging data for TensorBoard
            _ = tf.summary.histogram('hidden1_weight', w1)
            _ = tf.summary.histogram('hidden1_bias', b1)

        with tf.name_scope('output'):
            w = tf.Variable(tf.zeros([hidden1_units,1]), name='weights')
            b = tf.Variable(tf.zeros([1]), name='biases')
            y = tf.nn.sigmoid(tf.matmul(hidden1, w) + b*mult)
    
            # Logging data for TensorBoard
            _ = tf.summary.histogram('output_weight', w)
            _ = tf.summary.histogram('output_bias', b)

        # Define loss and optimizer
        with tf.name_scope('optimizer'):
            y_ = tf.placeholder(tf.float32, [None, 1])
            log_probability = tf.reduce_sum(y_*tf.log(y) + (1-y_)*tf.log(1-y))
            train_step = tf.train.GradientDescentOptimizer(0.01).minimize(-log_probability)
            correct_prediction = tf.equal(tf.sign(y-0.5), tf.sign(y_-0.5))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        # Logging data for TensorBoard
        _ = tf.summary.histogram('probability of training data', y)
        _ = tf.summary.scalar('log_probability', log_probability)
        _ = tf.summary.scalar('accuracy', accuracy)

        # plot image
        body = tf.placeholder(tf.string)
        image = tf.image.decode_png(body, channels=4)
        image = tf.expand_dims(image, 0)
        plot_image_summary = tf.summary.image('hidden1', image, max_outputs=1)

        # Train
        with tf.Session() as sess:
            writer = tf.summary.FileWriter('logs/nn_single', graph_def=sess.graph_def)
            sess.run(tf.initialize_all_variables())
            for i in range(500):
                batch_xs, batch_ys = dataset.next_batch(100)
                feed = {x: batch_xs, y_: batch_ys}
                sess.run(train_step, feed_dict=feed)
            
                if i % 10 == 0:
                    feed = {x: dataset.test_data, y_: dataset.test_label}
                    summary_str, lp, acc = sess.run(
                        [tf.summary.merge_all(), log_probability, accuracy],
                        feed_dict=feed)
                    writer.add_summary(summary_str, i)
                    print('LogProbability and Accuracy at step %s: %s, %s' % (i, lp, acc))

            plot_result(sess, dataset, x, y, subplot1, subplot2,
                        w1.eval(), b1.eval(), mult)
            for h in range(hidden1_units):
                plot_image = plot(dataset, [{
                    'a': w1[0, h] / w1[1, h],
                    'b': b1[h],
                    'name': 'hidden1_{}'.format(h)
                }])
                summary = sess.run(plot_image_summary, feed_dict={
                    'title': 'hidden1_{}'.format(h),
                    'body': plot_image.getvalue()
                })
                writer.add_summary(summary, 499)


def double_layer(dataset, subplot1, subplot2):
    with tf.Graph().as_default():
        mult = dataset.train_data.flatten().mean()
        hidden1_units = 2
        hidden2_units = 2
        
        # Create the model
        with tf.name_scope('input'):
            x = tf.placeholder(tf.float32, [None, 2])
    
        with tf.name_scope('hidden1'):
            w1 = tf.Variable(
                tf.truncated_normal([2, hidden1_units],stddev=1.0/math.sqrt(2.0)),
                name='weights')
            b1 = tf.Variable(tf.zeros([1, hidden1_units]), name='biases')
            hidden1 = tf.nn.tanh(tf.matmul(x, w1) + b1*mult)

            # Logging data for TensorBoard
            _ = tf.summary.histogram('hidden1_weight', w1)
            _ = tf.summary.histogram('hidden1_bias', b1)

        with tf.name_scope('hidden2'):
            w2 = tf.Variable(
                tf.truncated_normal([hidden1_units, hidden2_units],
                                    stddev=1.0/math.sqrt(2.0)),
                                    name='weights')
            b2 = tf.Variable(tf.zeros([1, hidden2_units]), name='biases')
            hidden2 = tf.nn.tanh(tf.matmul(hidden1, w2) + b2)
    
            # Logging data for TensorBoard
            _ = tf.summary.histogram('hidden2_weight', w2)
            _ = tf.summary.histogram('hidden2_bias', b2)
    
        with tf.name_scope('output'):
            w = tf.Variable(tf.zeros([hidden2_units,1]), name='weights')
            b = tf.Variable(tf.zeros([1]), name='biases')
            y = tf.nn.sigmoid(tf.matmul(hidden2, w) + b*mult)
    
            # Logging data for TensorBoard
            _ = tf.summary.histogram('output_weight', w)
            _ = tf.summary.histogram('output_bias', b)
    
        # Define loss and optimizer
        with tf.name_scope('optimizer'):
            y_ = tf.placeholder(tf.float32, [None, 1])
            log_probability = tf.reduce_sum(y_*tf.log(y) + (1-y_)*tf.log(1-y))
            train_step = tf.train.GradientDescentOptimizer(0.01).minimize(-log_probability)
            correct_prediction = tf.equal(tf.sign(y-0.5), tf.sign(y_-0.5))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        
        # Logging data for TensorBoard
        _ = tf.summary.histogram('probability of training data', y)
        _ = tf.summary.scalar('log_probability', log_probability)
        _ = tf.summary.scalar('accuracy', accuracy)

        # Train
        with tf.Session() as sess:
            writer = tf.summary.FileWriter('logs/nn_double', graph_def=sess.graph_def)
            sess.run(tf.initialize_all_variables())
            for i in range(500):
                batch_xs, batch_ys = dataset.next_batch(100)
                feed = {x: batch_xs, y_: batch_ys}
                sess.run(train_step, feed_dict=feed)
            
                if i % 10 == 0:
                    feed = {x: dataset.test_data, y_: dataset.test_label}
                    summary_str, lp, acc = sess.run(
                        [tf.summary.merge_all(), log_probability, accuracy],
                        feed_dict=feed)
                    writer.add_summary(summary_str, i)
                    print('LogProbability and Accuracy at step %s: %s, %s' % (i, lp, acc))
        
            plot_result(sess, dataset, x, y, subplot1, subplot2,
                        w1.eval(), b1.eval(), mult)

# Main
if __name__ == '__main__':
    fig = plt.figure()
    dataset = Dataset() 
    subplot1 = fig.add_subplot(2,2,1)
    subplot2 = fig.add_subplot(2,2,2)
    single_layer(dataset, subplot1, subplot2)

    subplot1 = fig.add_subplot(2,2,3)
    subplot2 = fig.add_subplot(2,2,4)
    double_layer(dataset, subplot1, subplot2)

    plt.show()

