import tensorflow as tf
import _pickle as pickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

np.random.seed(11)
tf.set_random_seed(11)

with open('mnist_simple.data', 'rb') as file:
  (train_data, train_label) = pickle.load(file)

def visualize(sess):
  fig1 = plt.figure()
  C = 12
  raw_data = train_data[:C]
  res = h_pool1_flat.eval(session=sess, feed_dict={x: train_data[:C]})
  res2 = h_fc1.eval(session=sess, feed_dict={x: train_data[:C]})
  res3 = y_conv.eval(session=sess, feed_dict={x: train_data[:C]})
  w = W_conv1.eval()

  vmax = np.max(res[:C].flatten())
  vmin = np.min(res[:C].flatten())

  # show filters
  for i in range(2):
    subplot = fig1.add_subplot(2+1, C+1, 1+(C+1)*(i+1))
    subplot.set_xticks([])
    subplot.set_yticks([])
    subplot.imshow(w[:,:,0,i], cmap=plt.cm.gray_r, interpolation='nearest')
  for c in range(C):
    # show raw data
    chars = res[c].reshape(14,14,2)
    subplot = fig1.add_subplot(2+1, C+1, c+2)
    subplot.set_xticks([])
    subplot.set_yticks([])
    subplot.imshow(raw_data[c].reshape(28,28),
                   vmax=1, vmin=0,
                   cmap=plt.cm.gray_r, interpolation='nearest')

    # show filtered data
    for i in range(2):
      subplot = fig1.add_subplot(2+1, C+1, 1+(C+1)*(i+1)+c+1)
      subplot.set_xticks([])
      subplot.set_yticks([])
      subplot.imshow(chars[:,:,i],
                     vmax=vmax, vmin=vmin,
                     cmap=plt.cm.gray_r, interpolation='nearest')

  fig2 = plt.figure()
  res2 = h_fc1.eval(session=sess, feed_dict={x: train_data})
  data0_x, data0_y = [], []
  data1_x, data1_y = [], []
  data2_x, data2_y = [], []
  subplot = fig2.add_subplot(1,1,1)
  for c, label in enumerate(train_label):
    if label[0]:
      data0_x.append(res2[c][0])
      data0_y.append(res2[c][1])
    if label[1]:
      data1_x.append(res2[c][0])
      data1_y.append(res2[c][1])
    if label[2]:
      data2_x.append(res2[c][0])
      data2_y.append(res2[c][1])
  subplot.scatter(data0_x, data0_y, marker='o', color='red')
  subplot.scatter(data1_x, data1_y, marker='o', color='blue')
  subplot.scatter(data2_x, data2_y, marker='o', color='green')
  plt.show()


def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1, seed=1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0, 1, shape=shape)
  return tf.Variable(initial)

def weight_constant(shape):
  form0 = np.array(
            [[ 2, 1, 0,-1,-2],
             [ 3, 2, 0,-2,-3],
             [ 4, 3, 0,-3,-4],
             [ 3, 2, 0,-2,-3],
             [ 2, 1, 0,-1,-2]]) / 25.0
  form1 = np.array(
            [[ 2, 3, 4, 3, 2],
             [ 1, 2, 3, 2, 1],
             [ 0, 0, 0, 0, 0],
             [-1,-2,-3,-2,-1],
             [-2,-3,-4,-3,-2]]) / 25.0
  form = np.zeros(5*5*1*2).reshape(5,5,1,2)
  form[:,:,0,0] = form0
  form[:,:,0,1] = form1
  return tf.constant(form, dtype=tf.float32)

def conv2d(x, W):
  return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

with tf.Graph().as_default():
  with tf.name_scope('input'):
    x = tf.placeholder(tf.float32, [None, 784], name='x-input')
    x_image = tf.reshape(x, [-1,28,28,1])
  
  with tf.name_scope('convolution'):
    W_conv1 = weight_constant([5,5,1,2])
    h_conv1 = tf.nn.relu(tf.abs(conv2d(x_image, W_conv1))-0.2)
    _ = tf.summary.histogram('h_conv1', h_conv1)
  
  with tf.name_scope('pooling'):
    h_pool1 = max_pool_2x2(h_conv1)
    h_pool1_flat = tf.reshape(h_pool1, [-1, 14*14*2])

  with tf.name_scope('features'):
    W_fc1 = weight_variable([14 * 14 * 2, 2])
    b_fc1 = bias_variable([2])
    h_fc1 = tf.nn.tanh(tf.matmul(h_pool1_flat, W_fc1) + b_fc1)
  
  with tf.name_scope('readout'):
    W_fc2 = weight_variable([2, 3])
    b_fc2 = bias_variable([3])
    y_conv=tf.nn.softmax(tf.matmul(h_fc1, W_fc2) + b_fc2)
  
  with tf.name_scope('optimizer'):
    y_ = tf.placeholder(tf.float32, [None, 3], name='y-input')
    cross_entropy = -tf.reduce_sum(y_*tf.log(y_conv))
    optimizer = tf.train.GradientDescentOptimizer(0.005)
    train_step = optimizer.minimize(cross_entropy)

    correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
  
  # Logging data for TensorBoard
  _ = tf.summary.scalar('cross entropy', cross_entropy)
  _ = tf.summary.scalar('accuracy', accuracy)
  
  with tf.Session() as sess:
    writer = tf.summary.FileWriter('/tmp/simple_cnn',
                                   graph_def=sess.graph_def)
    sess.run(tf.global_variables_initializer())
    for i in range(51):
      batch = [train_data, train_label]
      train_step.run(feed_dict={x: batch[0], y_: batch[1]})
      summary_str, acc = sess.run([tf.summary.merge_all(), accuracy],
                                  feed_dict={x: batch[0], y_: batch[1]})
      writer.add_summary(summary_str, i)
      print("step %d, training accuracy %g" % (i, acc))
    visualize(sess)

