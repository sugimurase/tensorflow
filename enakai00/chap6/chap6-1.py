from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
#import cPickle as pickle
import _pickle as pickle
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
np.random.seed(1)

candidates = [
1,2,3,4,7,8,11,13,15,21,23,26,32,35,39,49,51,52,59,60,62,64,69,75,77,
81,82,85,86,89,90,92,94,95,97,100,107,108,111,113,114,115,120,122,123,
124,126,128,130,131,133,134,135,138,139,146,156,161,165,173,176,177,178,
182,183,184,185,187,188,189,191,193,195,197,198,201,202,203,206,209]

img_data, img_label = [], []
n = 0
c = 0
while(c < len(candidates)):
  imgs, labels = mnist.train.next_batch(1)
  img = imgs[0]
  label = labels[0]
  if label[1] != 1:
    continue
  n += 1
  if n in candidates:
    img0 = img
    img1 = np.transpose(img.reshape(28,28)).flatten()
    img2 = np.array([max(img0[x], img1[x]) for x in range(28*28)])
    img_data.append(img0)
    img_label.append([1,0,0])
    img_data.append(img1)
    img_label.append([0,1,0])
    img_data.append(img2)
    img_label.append([0,0,1])
    c += 1

images = np.c_[np.array(img_label), np.array(img_data)]
np.random.shuffle(images)
img_label, img_data = np.hsplit(images,[3])

with open('mnist_simple.data', 'wb') as file:
      pickle.dump((img_data, img_label), file)

fig = plt.figure()
for i, img in enumerate(img_data):
  i += 1
  subplot = fig.add_subplot(12,20,i)
  subplot.set_xticks([])
  subplot.set_yticks([])
  subplot.imshow(img.reshape(28,28), vmax=1, vmin=0,
                 cmap=plt.cm.gray_r, interpolation='nearest')
plt.savefig('1.png')

