import tensorflow as tf
#import numpy as np


# Graph
graph = tf.Graph()
with graph.as_default():
    x = tf.placeholder(tf.float32, shape=(1, ), name='x')
    
    target = tf.constant([0.])
    
    l2_y_vals = tf.square(target - x)
    #tf.summary.scalar('l2_y_vals', l2_y_vals)
    
    init = tf.global_variables_initializer()
    
    # merge summaries
    merged_summary = tf.summary.merge_all()

# Data
graph_data = tf.Graph()
with graph_data.as_default():
    x_values = list(tf.split(tf.lin_space(-1., 1., 500), 500))

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph_data) as sess:
    x_values = sess.run(x_values)

with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    #print(x_values)
    # write learning loop here.
    i = 0
    for x_val in x_values:
        result = sess.run(l2_y_vals, feed_dict={x: x_val})
        #summary, result = sess.run([merged_summary, l2_y_vals], feed_dict={x: x_val})
        #writer.add_summary(summary, i)
        print(result)
        i += 1

# close writer
writer.close()
