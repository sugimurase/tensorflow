import tensorflow as tf
import numpy as np


# Graph
graph = tf.Graph()
with graph.as_default():
    with tf.name_scope('input'):
        # input 1x4x4x1
        x = tf.placeholder(tf.float32, shape=[1, 4, 4, 1], name="x")  # for image

    with tf.name_scope('convolv'):
        # convolv 1x4x4x1 -> 1x2x2x1
        my_filter = tf.constant(0.25, shape=[2, 2, 1, 1])
        my_strides = [1, 2, 2, 1]
        mov_avg_layer = tf.nn.conv2d(x, my_filter, my_strides, padding='SAME',
            name='MobingAvgWindow')

    with tf.name_scope('custom_layer'):
        # operation 1x2x2x1 -> 2x2
        input_matrix_sqeezed = tf.squeeze(mov_avg_layer)
        A = tf.constant([[1., 2.], [-1., 3.]])
        b = tf.constant(1., shape=[2, 2])
        custom_layer = tf.sigmoid(A * input_matrix_sqeezed + b)

    init = tf.global_variables_initializer()

# Data
# write data here.
x_value = np.random.uniform(size=[1, 4, 4, 1])

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    # write learning loop here.
    #for x_val in x_values:
    #    result = sess.run(add1, feed_dict={x: x_val})
    #    print(result)
    result = sess.run(custom_layer, feed_dict={x: x_value})
    print(result)

# close writer
writer.close()
