import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn import datasets
import io


# HyperParamters
# write parameters here.
learning_rate = 0.05
batch_size = 25
train_steps = 100

# Data
iris = datasets.load_iris()
x_vals = np.array([x[3] for x in iris.data])  # width
y_vals = np.array([y[0] for y in iris.data])  # length


# plot
def plot(xs, ys, a, b):
    best_fit = []
    for i in xs:
        y = a * i + b
        best_fit.append(y.reshape(-1, ))
    plt.plot(xs, ys, 'o', label='Data Points')
    plt.plot(xs, best_fit, 'r-', label='Best fit line', linewidth=3)
    plt.legend(loc='upper left')
    plt.title('Sepal Length vs Pedal Width')
    plt.xlabel('Pedal Width')
    plt.ylabel('Sepal Length')
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    return buf
    
    
# Graph
graph = tf.Graph()
with graph.as_default():
        
    with tf.name_scope('input'):
        x_data = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        
    with tf.name_scope('teacher'):
        y_target = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    
    with tf.name_scope('model'):
        with tf.name_scope('hidden'):
            A = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('A', A)
            b = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('b', b)
        model_output = tf.add(tf.matmul(x_data, A), b)
    
    with tf.name_scope('loss'):
        loss = tf.reduce_mean(tf.square(y_target - model_output))
        tf.summary.scalar('loss', loss)
    
    with tf.name_scope('optimizer'):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    
    with tf.name_scope('train'):
        train_step = optimizer.minimize(loss)
    
    with tf.name_scope('evaluate'):        
        correct_prediction = tf.equal(tf.argmax(model_output, 1), tf.argmax(y_target, 1))
        # correct_prediction = tf.equal(tf.sign(model_output - 0.5), tf.sign(y_target - 0.5))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar('accuracy', accuracy)
        
    with tf.name_scope('initialize'):
        init = tf.global_variables_initializer()
    
    merged_summary = tf.summary.merge_all()


graph_plot = tf.Graph()
with graph_plot.as_default():
    plot_image = tf.placeholder(tf.string)
    image = tf.image.decode_png(plot_image, channels=4)
    image = tf.expand_dims(image, 0)
    plot_image_summary = tf.summary.image('fig1', image, max_outputs=1)
    
    
# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    # write learning loop here.
    for i in range(train_steps):
        random_index = np.random.choice(len(x_vals), size=batch_size)
        random_x = np.transpose([x_vals[random_index]])
        random_y = np.transpose([y_vals[random_index]])
        summary, result = sess.run([merged_summary, train_step], feed_dict={x_data: random_x, y_target: random_y})
        writer.add_summary(summary, i)
        
    A_result = A.eval()
    b_result = b.eval()
    
with tf.Session(graph=graph_plot) as sess:
    buf = plot(x_vals, y_vals, A_result, b_result)
    plot_image_str = buf.getvalue()
    summary = sess.run(plot_image_summary, feed_dict={plot_image: plot_image_str})
    writer.add_summary(summary, 0)

# close writer
writer.close()
