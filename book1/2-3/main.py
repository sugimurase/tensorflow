import tensorflow as tf
import numpy as np


# Graph
graph = tf.Graph()
with graph.as_default():
    x = tf.placeholder(tf.float32, shape=(3, 5), name='x')
    m1 = tf.constant([[1.], [0.], [-1.], [2.], [4.]], name='m1')
    m2 = tf.constant([[2.]], name='m2')
    a1 = tf.constant([[10.]], name='a1')
    prod1 = tf.matmul(x, m1, name='prod1')
    prod2 = tf.matmul(prod1, m2, name='prod2')
    add1 = tf.add(prod2, a1)
    
    init = tf.global_variables_initializer()

# Data
x_value = np.array([[1., 3., 5., 7., 9.],
                    [-2., 0., 2., 4., 6.],
                    [-6., -3., 0., 3., 6.]])
x_values = np.array([x_value, x_value + 1])

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    for x_val in x_values:
        result = sess.run(add1, feed_dict={x: x_val})
        print(result)

# close writer
writer.close()
