import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from sklearn import datasets
import io


# HyperParamters
# write parameters here.
learning_rate = 0.05
batch_size = 25
train_steps = 100

# Data
iris = datasets.load_iris()
x_vals = np.array([x[3] for x in iris.data])  # width
y_vals = np.array([y[0] for y in iris.data])  # length


# plot
def plot(xs, ys, params):
    plt.plot(xs, ys, 'o', label='Data Points')
    for p in params:
        best_fit = []
        a = p['a']
        b = p['b']
        name = p['name']
        for i in xs:
            y = a * i + b
            best_fit.append(y.reshape(-1, ))
        plt.plot(xs, best_fit, '-', label='Best fit line {}'.format(name), linewidth=3)
    plt.legend(loc='upper left')
    plt.title('Sepal Length vs Pedal Width')
    plt.xlabel('Pedal Width')
    plt.ylabel('Sepal Length')
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    return buf
    
    
# Graph
graph_l1 = tf.Graph()
with graph_l1.as_default():
        
    with tf.name_scope('input'):
        x_data_l1 = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        
    with tf.name_scope('teacher'):
        y_target_l1 = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    
    with tf.name_scope('model'):
        with tf.name_scope('hidden'):
            A_l1 = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('A', A_l1)
            b_l1 = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('b', b_l1)
        model_output_l1 = tf.add(tf.matmul(x_data_l1, A_l1), b_l1)
    
    with tf.name_scope('loss'):
        loss_l1 = tf.reduce_mean(tf.abs(y_target_l1 - model_output_l1))
        tf.summary.scalar('loss', loss_l1)
    
    with tf.name_scope('optimizer'):
        optimizer_l1 = tf.train.GradientDescentOptimizer(learning_rate)
    
    with tf.name_scope('train'):
        train_step_l1 = optimizer_l1.minimize(loss_l1)
    
    with tf.name_scope('evaluate'):        
        correct_prediction_l1 = tf.equal(tf.argmax(model_output_l1, 1), tf.argmax(y_target_l1, 1))
        # correct_prediction = tf.equal(tf.sign(model_output - 0.5), tf.sign(y_target - 0.5))
        accuracy_l1 = tf.reduce_mean(tf.cast(correct_prediction_l1, tf.float32))
        tf.summary.scalar('accuracy', accuracy_l1)
        
    with tf.name_scope('initialize'):
        init_l1 = tf.global_variables_initializer()
    
    merged_summary_l1 = tf.summary.merge_all()
    
    
graph_l2 = tf.Graph()
with graph_l2.as_default():
        
    with tf.name_scope('input'):
        x_data_l2 = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        
    with tf.name_scope('teacher'):
        y_target_l2 = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    
    with tf.name_scope('model'):
        with tf.name_scope('hidden'):
            A_l2 = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('A', A_l2)
            b_l2 = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('b', b_l2)
        model_output_l2 = tf.add(tf.matmul(x_data_l2, A_l2), b_l2)
    
    with tf.name_scope('loss'):
        loss_l2 = tf.reduce_mean(tf.square(y_target_l2 - model_output_l2))
        tf.summary.scalar('loss', loss_l2)
    
    with tf.name_scope('optimizer'):
        optimizer_l2 = tf.train.GradientDescentOptimizer(learning_rate)
    
    with tf.name_scope('train'):
        train_step_l2 = optimizer_l2.minimize(loss_l2)
    
    with tf.name_scope('evaluate'):        
        correct_prediction_l2 = tf.equal(tf.argmax(model_output_l2, 1), tf.argmax(y_target_l2, 1))
        # correct_prediction = tf.equal(tf.sign(model_output - 0.5), tf.sign(y_target - 0.5))
        accuracy_l2 = tf.reduce_mean(tf.cast(correct_prediction_l2, tf.float32))
        tf.summary.scalar('accuracy', accuracy_l2)
        
    with tf.name_scope('initialize'):
        init_l2 = tf.global_variables_initializer()
    
    merged_summary_l2 = tf.summary.merge_all()
    
    
graph_demming = tf.Graph()
with graph_demming.as_default():
        
    with tf.name_scope('input'):
        x_data_demming = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        
    with tf.name_scope('teacher'):
        y_target_demming = tf.placeholder(shape=[None, 1], dtype=tf.float32)
    
    with tf.name_scope('model'):
        with tf.name_scope('hidden'):
            A_demming = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('A', A_demming)
            b_demming = tf.Variable(tf.random_normal(shape=[1, 1]))
            tf.summary.histogram('b', b_demming)
        model_output_demming = tf.add(tf.matmul(x_data_demming, A_demming), b_demming)
    
    with tf.name_scope('loss'):
        demming_numerator = tf.abs(tf.subtract(y_target_demming, model_output_demming))
        demming_denominator = tf.sqrt(tf.add(tf.square(A_demming), 1))
        loss_demming = tf.reduce_mean(tf.truediv(demming_numerator, demming_denominator))
        tf.summary.scalar('loss', loss_demming)
    
    with tf.name_scope('optimizer'):
        optimizer_demming = tf.train.GradientDescentOptimizer(learning_rate)
    
    with tf.name_scope('train'):
        train_step_demming = optimizer_demming.minimize(loss_demming)
    
    with tf.name_scope('evaluate'):
        correct_prediction_demming = tf.equal(tf.argmax(model_output_demming, 1), tf.argmax(y_target_demming, 1))
        # correct_prediction = tf.equal(tf.sign(model_output - 0.5), tf.sign(y_target - 0.5))
        accuracy_demming = tf.reduce_mean(tf.cast(correct_prediction_demming, tf.float32))
        tf.summary.scalar('accuracy', accuracy_demming)
        
    with tf.name_scope('initialize'):
        init_demming = tf.global_variables_initializer()
    
    merged_summary_demming = tf.summary.merge_all()


graph_plot = tf.Graph()
with graph_plot.as_default():
    plot_image = tf.placeholder(tf.string)
    image = tf.image.decode_png(plot_image, channels=4)
    image = tf.expand_dims(image, 0)
    plot_image_summary = tf.summary.image('fig1', image, max_outputs=1)
    
    
# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)

# Session
writer = tf.summary.FileWriter('{}/L1'.format(logdir), graph_l1)

with tf.Session(graph=graph_l1) as sess:
    sess.run(init_l1)
    
    # write learning loop here.
    for i in range(train_steps):
        random_index = np.random.choice(len(x_vals), size=batch_size)
        random_x = np.transpose([x_vals[random_index]])
        random_y = np.transpose([y_vals[random_index]])
        summary, result = sess.run([merged_summary_l1, train_step_l1],
            feed_dict={x_data_l1: random_x, y_target_l1: random_y})
        writer.add_summary(summary, i)
        
    A_l1_result = A_l1.eval()
    b_l1_result = b_l1.eval()
    
writer.close()

writer = tf.summary.FileWriter('{}/L2'.format(logdir), graph_l2)

with tf.Session(graph=graph_l2) as sess:
    sess.run(init_l2)
    
    # write learning loop here.
    for i in range(train_steps):
        random_index = np.random.choice(len(x_vals), size=batch_size)
        random_x = np.transpose([x_vals[random_index]])
        random_y = np.transpose([y_vals[random_index]])
        summary, result = sess.run([merged_summary_l2, train_step_l2],
            feed_dict={x_data_l2: random_x, y_target_l2: random_y})
        writer.add_summary(summary, i)
        
    A_l2_result = A_l2.eval()
    b_l2_result = b_l2.eval()
    
writer.close()

writer = tf.summary.FileWriter('{}/Demming'.format(logdir), graph_demming)

with tf.Session(graph=graph_demming) as sess:
    sess.run(init_demming)
    
    # write learning loop here.
    for i in range(train_steps):
        random_index = np.random.choice(len(x_vals), size=batch_size)
        random_x = np.transpose([x_vals[random_index]])
        random_y = np.transpose([y_vals[random_index]])
        summary, result = sess.run([merged_summary_demming, train_step_demming],
            feed_dict={x_data_demming: random_x, y_target_demming: random_y})
        writer.add_summary(summary, i)
        
    A_demming_result = A_demming.eval()
    b_demming_result = b_demming.eval()
    
writer.close()

writer = tf.summary.FileWriter('{}/Data'.format(logdir), graph_plot)

with tf.Session(graph=graph_plot) as sess:
    buf = plot(x_vals, y_vals, [
        {'a': A_l1_result, 'b': b_l1_result, 'name': 'L1'},
        {'a': A_l2_result, 'b': b_l2_result, 'name': 'L2'},
        {'a': A_demming_result, 'b': b_demming_result, 'name': 'Demming'}])
    plot_image_str = buf.getvalue()
    summary = sess.run(plot_image_summary, feed_dict={plot_image: plot_image_str})
    writer.add_summary(summary, 0)

writer.close()
