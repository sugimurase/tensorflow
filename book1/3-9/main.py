import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv


# HyperParamters
batch_size = 25
learning_rate = 0.01
train_steps = 1500


# Data
# - download data to csv
datadir = 'datadir'
datafile = '{}/birthweight.dat'.format(datadir)
if not tf.gfile.Exists(datadir):
    tf.gfile.MakeDirs(datadir)
if not tf.gfile.Exists(datafile):
    import requests
    dataurl = 'https://github.com/nfmcclure/tensorflow_cookbook/raw/master/01_Introduction/07_Working_with_Data_Sources/birthweight_data/birthweight.dat'
    birth_file = requests.get(dataurl)
    birth_data = birth_file.text.split('\r\n')
    birth_header = birth_data[0].split('\t')
    birth_data = [[float(x) for x in y.split('\t') if len(x) >= 1] for y in birth_data[1:] if len(y) >= 1]
    with open(datafile, 'w') as f:
        writer = csv.writer(f)
        writer.writerows(birth_data)
# - load data from csv
birth_data = []
with open(datafile, 'r', newline='') as csvfile:
    csv_reader = csv.reader(csvfile)
    birth_header = next(csv_reader)
    for row in csv_reader:
        birth_data.append(row)
birth_data = [[float(x) for x in row] for row in birth_data]
y_vals = np.array([x[1] for x in birth_data])
x_vals = np.array([x[2:9] for x in birth_data])
# - split data to train and test
train_indices = np.random.choice(len(x_vals), round(len(x_vals) * 0.8), replace=False)
test_indices = np.array(list(set(range(len(x_vals))) - set(train_indices)))
x_vals_train = x_vals[train_indices]
y_vals_train = y_vals[train_indices]
x_vals_test = x_vals[test_indices]
y_vals_test = y_vals[test_indices]
# - function for normalize data
def normalize_cols(m):
    col_max = m.max(axis=0)
    col_min = m.min(axis=0)
    return (m - col_min) / (col_max - col_min)
# - normalize data
x_vals_train = np.nan_to_num(normalize_cols(x_vals_train))
x_vals_test = np.nan_to_num(normalize_cols(x_vals_test))

# Graph
graph = tf.Graph()
with graph.as_default():
    
    with tf.name_scope('input'):
        x = tf.placeholder(shape=[None, 7], dtype=tf.float32)
    
    with tf.name_scope('teacher'):
        y_ = tf.placeholder(shape=[None, 1], dtype=tf.float32)
        
    with tf.name_scope('model'):
        W = tf.Variable(tf.random_normal(shape=[7, 1]), name='W')
        tf.summary.histogram('W', W)
        b = tf.Variable(tf.random_normal(shape=[1, 1]), name='b')
        tf.summary.histogram('b', b)
        y = tf.sigmoid(tf.add(tf.matmul(x, W), b, name='output'))
        tf.summary.histogram('y', y)
    
    with tf.name_scope('loss'):
        loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=y, labels=y_))
        tf.summary.scalar('loss', tf.reshape(loss, []))
    
    with tf.name_scope('optimizer'):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    
    with tf.name_scope('train'):
        train_step = optimizer.minimize(loss)
        
    with tf.name_scope('evaluate'):
        prediction = tf.round(tf.sigmoid(y))
        predictions_correct = tf.cast(tf.equal(prediction, y_), tf.float32)
        accuracy_for_train = tf.reduce_mean(predictions_correct)
        #tf.summary.scalar('accuracy_for_train', accuracy_for_train)
        accuracy_for_test = tf.reduce_mean(predictions_correct)
        #tf.summary.scalar('accuracy_for_test', accuracy_for_test)
        
    with tf.name_scope('initialize'):
        init = tf.global_variables_initializer()
        
    merged_summary = tf.summary.merge_all()


# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    # write learning loop here.
    for i in range(train_steps):
        rand_index = np.random.choice(len(x_vals_train), size=batch_size)
        rand_x = x_vals_train[rand_index]
        rand_y = np.transpose([y_vals_train[rand_index]])
        summary, _ = sess.run([merged_summary, train_step], feed_dict={x: rand_x, y_: rand_y})
        if i % 10 == 0:
            writer.add_summary(summary, i)
            #acc_train = sess.run(accuracy_for_train, feed_dict={x: x_vals_train, y_: np.transpose([y_vals_train])})
            #acc_test = sess.run(accuracy_for_test, feed_dict={x: x_vals_test, y_: np.transpose([y_vals_test])})
            #print('acc train: {}, test: {}'.format(acc_train, acc_test))

# close writer
writer.close()
