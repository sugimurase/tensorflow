import tensorflow as tf


# Graph
graph = tf.Graph()
with graph.as_default():
    x_data = tf.placeholder(tf.float32, name='x')
    m_const = tf.constant(3., name='m')
    my_product = tf.multiply(x_data, m_const, name='model')
    
    init = tf.global_variables_initializer()

# Data
x = [1., 3., 5., 7., 9.]

# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    for i in range(len(x)):
        result = sess.run(my_product, feed_dict={x_data: x[i]})
        print(result)

# close writer
writer.close()
