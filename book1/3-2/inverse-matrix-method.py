import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# Plotter
def plot(x_vals, y_vals, slope, y_intercept):
    best_fit = []
    for i in x_vals:
        best_fit.append(slope * i + y_intercept)
    
    plt.plot(x_vals, y_vals, 'o', label='Data')
    plt.plot(x_vals, best_fit, 'r-', label='Best fit line', linewidth=3)
    plt.legend(loc='upper left')
    plt.savefig('out.png')


# Data
datacount = 10000

# - generate data
x_vals = np.linspace(0, 10, datacount)
y_vals = x_vals + np.random.normal(0, 1, datacount)

# - generate design matrix A
x_vals_column = np.transpose(np.matrix(x_vals))
ones_column = np.transpose(np.matrix(np.repeat(1, datacount)))
A = np.column_stack((x_vals_column, ones_column))

# - generate bias b
b = np.transpose(np.matrix(y_vals))


# Graph
graph = tf.Graph()
with graph.as_default():
    A_tensor = tf.constant(A)
    b_tensor = tf.constant(b)
    
    tA = tf.transpose(A_tensor)
    tA_A = tf.matmul(tA, A_tensor)
    tA_A_inv = tf.matrix_inverse(tA_A)
    product = tf.matmul(tA_A_inv, tA)
    solution = tf.matmul(product, b_tensor)
    
    init = tf.global_variables_initializer()


# prepare directory for TensorBoard logs, and open writer
logdir = 'logs'
if tf.gfile.Exists(logdir):
    tf.gfile.DeleteRecursively(logdir)
tf.gfile.MakeDirs(logdir)
writer = tf.summary.FileWriter(logdir, graph)

# Session
with tf.Session(graph=graph) as sess:
    sess.run(init)
    
    solution_eval = sess.run(solution)
    
    slope = solution_eval[0][0]
    y_intercept = solution_eval[1][0]
    
    print('slope: {}'.format(str(slope)))
    print('y_intercept: {}'.format(str(y_intercept)))
    
    plot(x_vals, y_vals, slope, y_intercept)

# close writer
writer.close()
