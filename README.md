# 概要

# ディレクトリ構成

```
/
    getstart/
```

## getstart

TensorFlow公式の GET STARTED の実習。

# 追加インストール

## mecab

## CRF++

## cabocha

~/src/cabocha-0.69.tar.bz2
cd ~/src

tar fx cabocha-0.69.tar.bz2
cd cabocha-0.69

export LDFLAGS="-L${HOME}/opt/CRF++/lib -L${HOME}/opt/mecab/lib"
export CPPFLAGS="-I${HOME}/opt/CRF++/include -I${HOME}/opt/mecab/include"
./configure \
    --prefix=${HOME}/opt/cabocha \
    --enable-utf8-only
make install

cd python
python setup.py build_ext
python setup.py install
