import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.family'] = 'IPAexGothic'
import matplotlib.pyplot as plt
import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.cabocha')
figure_out = os.path.join(dir, './out.png')


class Morph:
    surface = None
    base = None
    pos = None
    pos1 = None
    
    def __init__(self, surface, base, pos, pos1):
        self.surface = surface
        self.base = base
        self.pos = pos
        self.pos1 = pos1


class Chunk:  # 文節クラス
    morphs = []  # 形態素（Morphオブジェクト）のリスト
    dst = None  # 係り先文節インデックス番号
    srcs = []  # 係り元文節インデックス番号のリスト
    
    def __init__(self, morphs, dst, srcs):
        self.morphs = morphs[:]
        self.dst = dst
        self.srcs = srcs[:]
    
    def appendsrc(self, src):
        self.srcs.append(src)
    

regex_cabochamark = re.compile(r'^\* (?P<src>[0-9]+) (?P<dst>[^D]+)D (?P<nodeio>[^ ]+) (?P<score>[\-0-9\.]+)\n')
regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    chunks = {}
    morphs = []
    dst = None
    src = None
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(morphs) > 0:
                    if src not in chunks:
                        chunks[src] = Chunk(morphs, dst, [])
                    morphs = []
                    dst = None
                    src = None
                if len(chunks) > 0:
                    for src in chunks:
                        if chunks[src].dst != '-1':
                            chunks[chunks[src].dst].appendsrc(src)
                    data.append(chunks)
                    chunks = {}
                continue
            m = regex_cabochamark.search(line)
            if m is not None:
                if len(morphs) > 0:
                    if src not in chunks:
                        chunks[src] = Chunk(morphs, dst, [])
                    morphs = []
                    dst = None
                    src = None
                dst = m.group('dst')
                src = m.group('src')
                continue
            m = regex_mecaboutput.search(line)
            morphs.append(Morph(m.group('surface'), m.group('base'),
                 m.group('pos'), m.group('pos1')))
        if len(morphs) > 0:
            if src not in chunks:
                chunks[src] = Chunk(morphs, dst, [])
            morphs = []
            dst = None
            src = None
        if len(chunks) > 0:
            for src in chunks:
                if chunks[src].dst != '-1':
                    chunks[chunks[src].dst].appendsrc(src)
            data.append(chunks)
            chunks = {}
    return data

def printchunks(data):
    for chunks in data:
        for src in chunks:
            print('{},{},{},{}'.format(src, chunks[src].dst, ' '.join(chunks[src].srcs), ''.join(map(lambda x: x.surface, chunks[src].morphs))))


if __name__ == '__main__':
    data = load()
    #print(len(data))
    printchunks(data[8:9])
