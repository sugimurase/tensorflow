import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.family'] = 'IPAexGothic'
import matplotlib.pyplot as plt
import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.cabocha')
figure_out = os.path.join(dir, './out.png')


class Morph:
    surface = None
    base = None
    pos = None
    pos1 = None
    
    def __init__(self, surface, base, pos, pos1):
        self.surface = surface
        self.base = base
        self.pos = pos
        self.pos1 = pos1


regex_cabochamark = re.compile(r'^\* (?P<nodeid>[0-9]+) (?P<nodename>[^ ]+) (?P<nodeio>[^ ]+) (?P<score>[\-0-9\.]+)\n')
regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    sentence = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(sentence) > 0:
                    data.append(sentence[:])
                    sentence = []
                continue
            m = regex_cabochamark.search(line)
            if m is not None:
                continue
            m = regex_mecaboutput.search(line)
            sentence.append(Morph(m.group('surface'), m.group('base'),
                 m.group('pos'), m.group('pos1')))
    if len(sentence) > 0:
        data.append(sentence[:])
        sentence = []
    return data

def printsentence(data):
    for d in data:
        for s in d:
            print('{},{},{},{}'.format(s.surface, s.base, s.pos, s.pos1))

if __name__ == '__main__':
    data = load()
    printsentence(data[2:3])
