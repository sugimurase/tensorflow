import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.mecab')


regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    sentence = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(sentence) > 0:
                    data.append(sentence[:])
                    sentence = []
                continue
            m = regex_mecaboutput.search(line)
            sentence.append({
                'surface': m.group('surface'),
                'pos': m.group('pos'),
                'pos1': m.group('pos1'),
                'base': m.group('base')
            })
    if len(sentence) > 0:
        data.append(sentence[:])
        sentence = []
    return data
    
def get_noun_and_nouns(data):
    res = []
    for s in data:
        ws = len(s)
        for i in range(ws - 2):
            if s[i]['pos'] == '名詞' and \
               s[i + 1]['pos'] == '助詞' and s[i + 1]['pos1'] == '連体化' and s[i + 1]['base'] == 'の' and \
               s[i + 2]['pos'] == '名詞':
                res.append('{}{}{}'.format(s[i]['base'], s[i + 1]['base'], s[i + 2]['base']))
    return res

if __name__ == '__main__':
    data = load()
    noun_and_nouns = get_noun_and_nouns(data)
    #for i in range(3):
    #    print(noun_and_nouns[i])
    print(noun_and_nouns)
