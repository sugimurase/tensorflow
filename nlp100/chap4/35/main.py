import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.mecab')


regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    sentence = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(sentence) > 0:
                    data.append(sentence[:])
                    sentence = []
                continue
            m = regex_mecaboutput.search(line)
            sentence.append({
                'surface': m.group('surface'),
                'pos': m.group('pos'),
                'pos1': m.group('pos1'),
                'base': m.group('base')
            })
    if len(sentence) > 0:
        data.append(sentence[:])
        sentence = []
    return data
    
def get_continuous_nouns(data):
    res = []
    for s in data:
        ws = []
        for w in s:
            if w['pos'] != '名詞':
                if len(ws) > 0:
                    res.append(''.join(ws))
                    ws = []
                continue
            ws.append(w['base'])
        if len(ws) > 0:
            res.append(''.join(ws))
            ws = []
    return res

if __name__ == '__main__':
    data = load()
    continuous_nouns = get_continuous_nouns(data)
    #for i in range(3):
    #    print(noun_and_nouns[i])
    print(continuous_nouns)
