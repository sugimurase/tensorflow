import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams['font.family'] = 'IPAexGothic'
import matplotlib.pyplot as plt
import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.mecab')
figure_out = os.path.join(dir, './out.png')


regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    sentence = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(sentence) > 0:
                    data.append(sentence[:])
                    sentence = []
                continue
            m = regex_mecaboutput.search(line)
            sentence.append({
                'surface': m.group('surface'),
                'pos': m.group('pos'),
                'pos1': m.group('pos1'),
                'base': m.group('base')
            })
    if len(sentence) > 0:
        data.append(sentence[:])
        sentence = []
    return data
    
def get_continuous_nouns(data):
    res = []
    for s in data:
        ws = []
        for w in s:
            if w['pos'] != '名詞':
                if len(ws) > 0:
                    res.append(''.join(ws))
                    ws = []
                continue
            ws.append(w['base'])
        if len(ws) > 0:
            res.append(''.join(ws))
            ws = []
    return res

def tally(data):
    ws = {}
    for d in data:
        if d not in ws:
            ws[d] = {
                'base': d,
                'count': 1
            }
        else:
            ws[d]['count'] += 1
    res = []
    for w in ws:
        res.append(ws[w])
    return sorted(res, key=lambda x: x['count'], reverse=True)
    
def histogram(data):
    hist = {}
    max_count = 0
    for d in data:
        if max_count < d['count']:
            max_count = d['count']
        if d['count'] not in hist:
            hist[d['count']] = 1
        else:
            hist[d['count']] += 1
    res = []
    for c in range(max_count):
        if c in hist:
            res.append(hist[c])
        else:
            res.append(0)
    return res

def plot(data):
    fig = plt.figure()
    subplot = fig.add_subplot(1, 1, 1)
    x_values = list(range(len(data)))
    y_values = list(map(lambda x: x, data))
    subplot.bar(x_values, y_values)
    plt.savefig(figure_out)

if __name__ == '__main__':
    data = load()
    data = get_continuous_nouns(data)
    data = tally(data)
    data = histogram(data)
    plot(data)
    print(data)
