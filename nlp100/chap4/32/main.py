import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../neko.txt.mecab')


regex_mecaboutput = re.compile(r'^(?P<surface>[^\t]+)\t(?P<pos>[^,]*),(?P<pos1>[^,]*),[^,]*,[^,]*,[^,]*,[^,]*,(?P<base>[^,]*).*\n$')
def load():
    data = []
    sentence = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            #print(line)
            if line == 'EOS\n':
                if len(sentence) > 0:
                    data.append(sentence[:])
                    sentence = []
                continue
            m = regex_mecaboutput.search(line)
            sentence.append({
                'surface': m.group('surface'),
                'pos': m.group('pos'),
                'pos1': m.group('pos1'),
                'base': m.group('base')
            })
    if len(sentence) > 0:
        data.append(sentence[:])
        sentence = []
    return data
    
def get_verb_bases(data):
    verb_bases = []
    for d in data:
        for w in d:
            if w['pos'] == '動詞':
                verb_bases.append(w['base'])
    return verb_bases

if __name__ == '__main__':
    data = load()
    verb_bases = get_verb_bases(data)
    for i in range(5):
        print(verb_bases[i])
