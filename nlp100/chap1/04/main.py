import re

s1 = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can." 

def pickup(s):
    l = re.split(r'[ ,\.]+', s)
    l = list(filter(lambda x: x != '', l))
    r = {}
    for i in range(len(l)):
        if (i + 1) in [1, 5, 6, 7, 8, 9, 15, 16, 19]:
            r['{}'.format(l[i][0])] = i + 1
        else:
            r['{}{}'.format(l[i][0], l[i][1])] = i + 1
    return r

if __name__ == '__main__':
    print(pickup(s1))
