import random
import re

s1 = "I couldn't believe that I could actually understand what I was reading : the phenomenal power of the human mind ." 
#s1 = 'こんちには みさなん おんげき ですか？ わしたは げんき です。'

def typoglycemia(s):
    l = re.split(r'[ ]+', s)
    l = list(filter(lambda x: x != '', l))
    res = []
    for s in l:
        if len(s) <= 4:
            res.append(s)
            continue
        cs = list(s)
        c0 = cs.pop(0)
        cN = cs.pop()
        random.shuffle(cs)
        cs.insert(0, c0)
        cs.append(cN)
        res.append(''.join(cs))
    return ' '.join(res)

if __name__ == '__main__':
    print(typoglycemia(s1))
