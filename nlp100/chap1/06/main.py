import re


s1 = 'paraparaparadise'
s2 = 'paragraph'

def ngram(s, n):
    # make word n-gram
    lw = re.split(r'[ ]+', s)
    ngram_w = []
    for i in range(len(lw) - n + 1):
        ngram_w.append(lw[i:i + n])
    
    # make character n-gram
    ngram_c = []
    for i in range(len(s) - n + 1):
        t = s[i:i + n]
        if t not in ngram_c:
            ngram_c.append(t)
        
    return {'w': ngram_w, 'c': ngram_c}

def or_set(x, y):
    res = {}
    for i in x:
        if i not in res:
            res[i] = ['x']
    for i in y:
        if i not in res:
            res[i] = ['y']
        else:
            res[i].append('y')
    return list(filter(lambda x: len(res[x]) > 0, res))

def and_set(x, y):
    res = {}
    for i in x:
        if i not in res:
            res[i] = ['x']
    for i in y:
        if i not in res:
            res[i] = ['y']
        else:
            res[i].append('y')
    return list(filter(lambda x: len(res[x]) == 2, res))

def sub_set(x, y):
    res = {}
    for i in x:
        if i not in res:
            res[i] = ['x']
    for i in y:
        if i not in res:
            res[i] = ['y']
        else:
            res[i].append('y')
    return list(filter(lambda x: 'y' not in res[x], res))

if __name__ == '__main__':
    x = ngram(s1, 2)
    y = ngram(s2, 2)
    
    print('x: {}'.format(x['c']))
    print('y: {}'.format(y['c']))
    print('X or Y: {}'.format(or_set(x['c'], y['c'])))
    print('X and Y: {}'.format(and_set(x['c'], y['c'])))
    print('X - Y: {}'.format(sub_set(x['c'], y['c'])))
    print('Y - X: {}'.format(sub_set(y['c'], x['c'])))

    print('X include "se": {}'.format('se' in x['c']))
    print('Y include "se": {}'.format('se' in y['c']))
    