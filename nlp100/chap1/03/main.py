import re

s1 = 'Now I need a drink, alcoholic of course, after the heavy lectures involving quantum mechanics.'

def splitandlen(s):
    l = re.split(r'[ ,\.]+', s)
    l = filter(lambda x: x != '', l)
    return list(map(lambda x: len(x), l))
    
if __name__ == '__main__':
    print(splitandlen(s1))
