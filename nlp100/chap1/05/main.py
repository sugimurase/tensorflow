import re


s1 = 'I am an NLPer'

def ngram(s, n):
    # make word n-gram
    lw = re.split(r'[ ]+', s)
    ngram_w = []
    for i in range(len(lw) - n + 1):
        ngram_w.append(lw[i:i + n])
    
    # make character n-gram
    ngram_c = []
    for i in range(len(s) - n + 1):
        ngram_c.append(s[i:i + n])
        
    return {'ngram_w': ngram_w, 'ngram_c': ngram_c}

if __name__ == '__main__':
    print(ngram(s1, 2))
