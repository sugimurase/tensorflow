import os


dir = os.path.dirname(__file__)
filename = os.path.join(dir, '../hightemp.txt')

def countline(filename):
    c = 0
    with open(filename, 'r') as f:
        for line in f:
            c += 1
    return c

if __name__ == '__main__':
    print(countline(filename))
