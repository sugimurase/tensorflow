import os
import re


dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../hightemp.txt')

def load():
    d = []
    with open(filename_in, 'r') as f:
        for line in f:
            (pref, city, temp, date) = re.split(r'\t', line)
            d.append({
                'pref': pref,
                'city': city,
                'temp': temp,
                'date': date
            })
    return d
    
def col1set(d):
    s = {}
    for row in d:
        if row['pref'] not in s:
            s[row['pref']] = 1
        else:
            s[row['pref']] += 1
    return s

def col1sort(d):
    return sorted(d.items(), key=lambda d: d[1], reverse=True)

if __name__ == '__main__':
    data = load()
    for item in col1sort(col1set(data)):
        print(item)
