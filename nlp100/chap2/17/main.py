import os
import re


dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../hightemp.txt')

def load():
    d = []
    with open(filename_in, 'r') as f:
        for line in f:
            d.append(re.split(r'\t', line))
    return d
    
def col1set(d):
    s = {}
    for cols in d:
        if cols[0] not in s:
            s[cols[0]] = 1
        else:
            s[cols[0]] += 1
    return s

def col1sort(colset):
    return sorted(colset.keys())

if __name__ == '__main__':
    data = load()
    for item in col1sort(col1set(data)):
        print(item)
