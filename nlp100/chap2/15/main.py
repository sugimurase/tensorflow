import os
from optparse import OptionParser


# parser
parser = OptionParser()
parser.add_option('-n',
    action='store', type='int', dest='n', default='10',
    help='number of lines')


dir = os.path.dirname(__file__)
filename = os.path.join(dir, '../hightemp.txt')

def countline():
    c = 0
    with open(filename, 'r') as f:
        for line in f:
            c += 1
    return c

def tail(n):
    c = countline()
    with open(filename, 'r') as f:
        m = 0
        for line in f:
            m += 1
            if c - m >= n:
                continue
            print(line, end='')

if __name__ == '__main__':
    options, args = parser.parse_args()
    tail(options.n)
