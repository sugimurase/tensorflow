import os
import re


dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../hightemp.txt')

def load():
    d = []
    with open(filename_in, 'r') as f:
        for line in f:
            d.append(re.split(r'\t', line))
    return d
    
def col3sort(data):
    return sorted(data, key=lambda d: d[2], reverse=True)

if __name__ == '__main__':
    data = load()
    for item in col3sort(data):
        print(item)
