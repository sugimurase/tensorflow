import os
from optparse import OptionParser


# parser
parser = OptionParser()
parser.add_option('-n',
    action='store', type='int', dest='n', default='10',
    help='number of lines')


dir = os.path.dirname(__file__)
filename = os.path.join(dir, '../hightemp.txt')

def head(c):
    with open(filename, 'r') as f:
        for line in f:
            print(line, end='')
            c -= 1
            if c <= 0:
                break

if __name__ == '__main__':
    options, args = parser.parse_args()
    head(options.n)
