import os
import re


dir = os.path.dirname(__file__)
filename = os.path.join(dir, '../hightemp.txt')

def replace(filename):
    l = ''
    with open(filename, 'r') as f:
        for line in f:
            l += re.sub(r'\t', ' ', line)
    return l

if __name__ == '__main__':
    print(replace(filename))
