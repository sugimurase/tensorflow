import os
import re


dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../hightemp.txt')
filename_col1 = os.path.join(dir, 'col1.txt')
filename_col2 = os.path.join(dir, 'col2.txt')

def splitdata(filename_in, filename_col1, filename_col2):
    l = ''
    f_col1 = open(filename_col1, 'w')
    f_col2 = open(filename_col2, 'w')
    with open(filename_in, 'r') as f:
        for line in f:
            cols = re.split(r'\t', line)
            f_col1.write('{}\n'.format(cols[0]))
            f_col2.write('{}\n'.format(cols[1]))
    f_col2.close()
    f_col1.close()

if __name__ == '__main__':
    print(splitdata(filename_in, filename_col1, filename_col2))
