import math
import os
from optparse import OptionParser


dir = os.path.dirname(__file__)
filename = os.path.join(dir, '../hightemp.txt')

def countline():
    c = 0
    with open(filename, 'r') as f:
        for line in f:
            c += 1
    return c

def split(n):
    c = countline()
    c0 = math.ceil(c / n)
    with open(filename, 'r') as f:
        for nx in range(n):
            with open('{}/splited-{}.txt'.format(dir, nx + 1), 'w') as fo:
                m = 0
                for line in f:
                    m += 1
                    if m < c0:
                        fo.write(line)
                    elif m == c0:
                        fo.write(line)
                        break

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-n',
        action='store', type='int', dest='n',
        help='number of lines')
    options, args = parser.parse_args()
    if options.n is None:
        parser.error('specify -n option')
    split(options.n)
