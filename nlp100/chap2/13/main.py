import os
import re


dir = os.path.dirname(__file__)
filename_col1 = os.path.join(dir, 'col1.txt')
filename_col2 = os.path.join(dir, 'col2.txt')
filename_out = os.path.join(dir, 'merged.txt')

def mergedata(filename_col1, filename_col2, filename_out):
    l = ''
    f_col1 = open(filename_col1, 'r')
    f_col2 = open(filename_col2, 'r')
    f_out = open(filename_out, 'w')
    l1 = f_col1.readline()
    while l1 != '':
        l1 = re.sub(r'\n$', '', l1)
        l2 = f_col2.readline()
        l2 = re.sub(r'\n$', '', l2)
        f_out.write('{}\t{}\n'.format(l1, l2))
        l1 = f_col1.readline()
    f_out.close()
    f_col2.close()
    f_col1.close()

if __name__ == '__main__':
    print(mergedata(filename_col1, filename_col2, filename_out))
