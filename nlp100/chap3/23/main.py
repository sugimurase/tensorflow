import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-UK.json')

regex_get_text = re.compile(r'^(?P<label>==+)(?P<text>.+)(?P=label)')
#regex_get_text = re.compile(r'^(?P<label>==+).*')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(line)
    return data

def filter_category(data):
    categories = []
    for d in data:
        m = regex_get_text.search(d)
        if m is not None:
            categories.append({
                'level': len(m.group('label')) - 1,
                'text': m.group('text')
            })
    return categories

if __name__ == '__main__':
    data = load()
    filtered_data = filter_category(data)
    print(filtered_data)
