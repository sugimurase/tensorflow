import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-UK.json')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(line)
    return data
    
def getText(data):
    return data

def filterCategory(data):
    category_texts = filter(lambda x: '[[Category:' in x, data)
    categories = map(lambda x: getText(x), category_texts)
    return list(categories)

if __name__ == '__main__':
    data = load()
    filtered_data = filterCategory(data)
    print(filtered_data)
