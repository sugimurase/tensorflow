import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-UK.json')

regex_get_text = re.compile(r'\[\[(?P<label>[^:]*):(?P<text>[^|]*).*\]\]')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(line)
    return data
    
def get_text(data):
    m = regex_get_text.search(data)
    print(m)
    return m.group('text')

def filter_category(data):
    category_texts = filter(lambda x: '[[Category:' in x, data)
    categories = map(lambda x: get_text(x), category_texts)
    return list(categories)

if __name__ == '__main__':
    data = load()
    filtered_data = filter_category(data)
    print(filtered_data)
