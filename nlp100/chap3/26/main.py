import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-UK.json')

regex_section = re.compile(r'\n(?P<section>{{基礎情報.*\n}})', re.DOTALL)
regex_template_values = re.compile(r'\n\|(?P<label>.*?)\s*=\s*(?P<value>.*?)(?=\n\||\n}})', re.DOTALL)
regex_emphasis = re.compile(r'\'\'\'(.*?)\'\'\'')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(line)
    return data

def remove_emphasis(data):
    return regex_emphasis.sub('', data)

def get_section(data):
    m = regex_section.search(''.join(data))
    if m is not None:
        return m.group('section')
    return None

def get_template_values(data):
    values = []
    m = regex_template_values.findall(data)
    for label, value in m:
        values.append({
            'label': label,
            'value': remove_emphasis(value)
        })
    return values

if __name__ == '__main__':
    data = load()
    section = get_section(data)
    template_values = get_template_values(section)
    for d in template_values:
        print(d['value'])
