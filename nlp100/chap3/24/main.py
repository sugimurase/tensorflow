import os
import re

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-UK.json')

regex_get_text = re.compile(r'(?:File|ファイル):(?P<filename>[^|]+)')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(line)
    return data
    
def to_filename(filename):
    return filename.replace(' ', '_')

def filter_category(data):
    categories = []
    for d in data:
        m = regex_get_text.search(d)
        if m is not None:
            #print(m, m.group('filename'))
            categories.append({
                #'original': d,
                'filename': to_filename(m.group('filename'))
            })
    return categories

if __name__ == '__main__':
    data = load()
    filtered_data = filter_category(data)
    print(filtered_data)
