import json
import os

dir = os.path.dirname(__file__)
filename_in = os.path.join(dir, '../jawiki-country.json')
filename_out = os.path.join(dir, '../jawiki-UK.json')

def load():
    data = []
    with open(filename_in, 'r', encoding='utf-8') as f:
        for line in f:
            data.append(json.loads(line))
    return data
    
def filterUK(data):
    return list(filter(lambda x: 'イギリス' in x['title'], data))

def save_to_file(data):
    with open(filename_out, 'w') as f:
        for d in data:
            f.write('{}\n'.format(d['text']))

if __name__ == '__main__':
    data = load()
    filtered_data = filterUK(data)
    print(filtered_data[0]['text'])
    save_to_file(filtered_data)
